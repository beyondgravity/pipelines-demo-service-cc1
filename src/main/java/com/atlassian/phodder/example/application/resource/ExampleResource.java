package com.atlassian.phodder.example.application.resource;

import com.atlassian.phodder.example.application.service.ExampleService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Resource for interacting with s via REST.
 */
@Path(ExampleResource.Resource.ROOT_PATH)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public final class ExampleResource {

    private final ExampleService exampleService;

    public ExampleResource(final ExampleService exampleService) {
        this.exampleService = exampleService;
    }

    @GET
    public String getExample() {
        return "Cat says meow. Bit says Bucket.";
    }

    /**
     * Provides references to the URIs exposed by the endpoint.
     */
    static final class Resource {
        private Resource() { }

        static final String ROOT_PATH = "/rest/example/";
    }
}
