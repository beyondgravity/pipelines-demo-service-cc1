package com.atlassian.phodder.example.application.service;

import com.atlassian.phodder.example.application.dao.ExampleDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link ExampleService}.
 */
public class ExampleServiceImpl implements ExampleService {
    private static final Logger log = LoggerFactory.getLogger(ExampleServiceImpl.class);

    private final ExampleDao exampleDao;

    public ExampleServiceImpl(final ExampleDao exampleDao) {
        this.exampleDao = exampleDao;
    }

    public String createMap(String key, String value) {
        return "hi";
    }

    public String getValue(String key) {
        return "bye";
    }
}
